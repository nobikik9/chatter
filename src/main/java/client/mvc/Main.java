package client.mvc;

import javax.swing.*;

import client.mvc.controllers.ChatterController;
import client.mvc.models.ChatterModel;
import client.mvc.views.ChatterView;

/**
 * Created by nobik on 5/1/16.
 */
public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            ChatterModel model = new ChatterModel();
            ChatterView view = new ChatterView();
            new ChatterController(model, view);
        });
    }
}
