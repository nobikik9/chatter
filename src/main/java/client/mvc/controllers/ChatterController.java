package client.mvc.controllers;

import client.mvc.models.ChatterModel;
import client.mvc.views.ChatterView;

/**
 * Created by nobik on 5/1/16.
 */

public class ChatterController {
    ChatterModel model;
    ChatterView view;

    public ChatterController(ChatterModel model, ChatterView view) {
        this.model = model;
        this.view = view;
        initListeners();
    }

    private void initListeners() {
        view.setConnectToServerListener(new ConnectToServerListener(model, view));
        view.setSendMessageListener(new SendMessageListener(model));

        model.setConnectionRefusedListener(new ConnectionRefusedListener(model, view));
        model.setProcessMessageListener(new ProcessMessageListener(view));
    }
}
