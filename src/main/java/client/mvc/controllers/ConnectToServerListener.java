package client.mvc.controllers;

import client.mvc.models.ChatterModel;
import client.mvc.models.ConnectToServerException;
import client.mvc.views.ChatterView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by nobik on 5/24/16.
 */
public class ConnectToServerListener implements ActionListener {
    private static final String ipError = "IP is not correct, so bad :(!";
    private static final String portErrorTitle = "Port is not correct!";
    private static final String portError = "Port must be a number between 1024 and 9999";
    private static final String nameErrorTitle = "Username is not current!";
    private static final String nameError = "Username must contain more then 2 symbols (and less then 10) and consists of English letters and/or digits";
    private static final String serverError = "Cannot connect to server";

    ChatterModel model;
    ChatterView view;

    public ConnectToServerListener(ChatterModel model, ChatterView view) {
        this.model = model;
        this.view = view;
    }

    public void actionPerformed(ActionEvent e) {
        String ip = view.getIP();
        String port = view.getPort();
        String userName = view.getUserName();

        if (!validateIP(ip)) {
            view.notifyError(ipError, "");
            return;
        }

        if (!validatePort(port)) {
            view.notifyError(portError, portErrorTitle);
            return;
        }


        if (!validateUserName(userName)) {
            view.notifyError(nameError, nameErrorTitle);
            return;
        }

        try {
            model.connectToServer(ip, Integer.valueOf(port), userName);
        } catch (ConnectToServerException e1) {
            e1.printStackTrace();
            view.notifyError(serverError + "\n" + e1.getMessage(), "");
            return;
        }

        // Connection was successful
        view.setRunningState();
    }

    public boolean validateIP(String ip) {
        boolean result;
        try {
            InetAddress inet = InetAddress.getByName(ip);
            System.out.println(inet.getHostAddress());
            result = inet.getHostAddress().equals(ip) && inet instanceof Inet4Address;
        } catch (UnknownHostException e) {
            result = false;
        }
        return result;
    }

    public boolean validatePort(String port) {
        Integer p;
        try {
            p = Integer.valueOf(port);
        } catch (ClassCastException|NumberFormatException e) {
            return false;
        }
        return p >= 1024 && p <= 9999;
    }

    public boolean validateUserName(String name) {
        return name.matches("[a-zA-Z0-9]{3,10}");
    }
}

