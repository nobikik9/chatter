package client.mvc.controllers;

import client.mvc.models.ChatterModel;
import client.mvc.views.ChatterView;

/**
 * Created by nobik on 5/31/16.
 */
public class ConnectionRefusedListener {
    final ChatterModel model;
    final ChatterView view;
    boolean refused = false;

    ConnectionRefusedListener(ChatterModel model, ChatterView view) {
        this.model = model;
        this.view = view;
    }

    public void connectionRefused() {
        if (refused) {
            return;
        }
        refused = true;
        System.out.println("There was a try to refuse connection!");
        view.notifyError("Connection was refused!", "Houston, we has a problem");
        view.setWaitingState();
        view.clearScreen();
        //System.exit(0);
    }
}
