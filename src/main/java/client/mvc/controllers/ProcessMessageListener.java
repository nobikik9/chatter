package client.mvc.controllers;

import client.mvc.views.ChatterView;
import communication.Message;

/**
 * Created by nobik on 5/31/16.
 */
public class ProcessMessageListener {
    private ChatterView view;

    ProcessMessageListener(ChatterView view) {
        this.view = view;
    }

    public void processMessage(Message message) {
        if (message == null)
            return;
        view.processMessage(message);
        System.out.println("There was a try to process message");
    }
}
