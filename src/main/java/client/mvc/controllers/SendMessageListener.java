package client.mvc.controllers;

import client.mvc.models.ChatterModel;
import communication.Message;

/**
 * Created by nobik on 5/31/16.
 */
public class SendMessageListener {
    ChatterModel model;

    SendMessageListener(ChatterModel model) {
        this.model = model;
    }

    public void sendMessage(Message message) {
        model.broadcast(message);
    }
}
