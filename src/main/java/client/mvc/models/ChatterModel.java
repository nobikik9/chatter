package client.mvc.models;

import client.mvc.controllers.ConnectionRefusedListener;
import client.mvc.controllers.ProcessMessageListener;
import communication.Message;
import communication.TextMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by nobik on 5/1/16.
 */
public class ChatterModel implements MessageWriter {
    private Socket socket;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private ListenerThread listener;
    private String ip;
    private Integer port;
    private String user;

    private ConnectionRefusedListener connectionRefusedListener;
    private ProcessMessageListener processMessageListener;

    public ChatterModel() {
    }

    public void connectToServer(String ip, Integer port, String user) throws ConnectToServerException {
        this.ip = ip;
        this.port = port;
        this.user = user;
        try {
            try {
                socket.close();
                inputStream.close();
                outputStream.close();
            } catch (Exception e) {}
            Thread socketThread = new Thread(() -> {
                try {
                    socket = new Socket(ip, port);
                } catch (IOException e) {
                    socket = null;
                }
            });
            socketThread.start();
            socketThread.join(1000);
            if (socket == null) {
                socketThread.interrupt();
                throw new IOException();
            }
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.flush();
            inputStream = new ObjectInputStream(socket.getInputStream());
            listener = new ListenerThread(inputStream, this);
            listener.setProcessMessageListener(processMessageListener);
            listener.start();
            broadcast(new TextMessage(user, "init", null, 0));
        } catch (IOException e) {
            throw new ConnectToServerException("Connection problem!");
        } catch (InterruptedException e) {
            throw new ConnectToServerException("Connection timed out!");
        }
    }

    public void setConnectionRefusedListener(ConnectionRefusedListener listener) {
        this.connectionRefusedListener = listener;
    }

    public void setProcessMessageListener(ProcessMessageListener processMessageListener) {
        this.processMessageListener = processMessageListener;
    }

    public boolean reconnect() {
        try {
            connectToServer(ip, port, user);
        } catch (ConnectToServerException e) {
            return false;
        }
        return true;
    }

    private int reconnectCount = 0;

    @Override
    public void broadcast(Message message) {
            try {
                outputStream.writeObject(message);
                outputStream.flush();
                reconnectCount = 0;
            } catch (IOException e) {
                if (reconnectCount < 10) {
                    ++reconnectCount;
                    if (reconnect()) {
                        broadcast(message);
                    }
                } else {
                    connectionRefused();
                    e.printStackTrace();
                }
            }
    }

    @Override
    public void connectionRefused() {
        try {
            // it will imply connectionRefusedListener
            try {
                socket.close();
            } catch (Exception e) {}
        } catch (Exception e) {}
        connectionRefusedListener.connectionRefused();
    }

    public ObjectInputStream getInputStream() {
        return inputStream;
    }
}
