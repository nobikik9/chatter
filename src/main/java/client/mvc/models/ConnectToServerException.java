package client.mvc.models;

import client.mvc.views.ConnectToServerWindow;

/**
 * Created by nobik on 5/18/16.
 */
public class ConnectToServerException extends Exception {
    public ConnectToServerException() {
        super();
    }

    public ConnectToServerException(String message) {
        super(message);
    }
}
