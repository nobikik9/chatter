package client.mvc.models;

import client.mvc.controllers.ProcessMessageListener;
import communication.Message;
import communication.SerializableImage;
import communication.TextMessage;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.SocketException;

/**
 * Created by nobik on 5/18/16.
 */
public class ListenerThread extends Thread {
    private ObjectInputStream inputStream;
    private MessageWriter writer;
    private ProcessMessageListener processMessageListener;

    public ListenerThread(ObjectInputStream inputStream, MessageWriter writer) {
        this.inputStream = inputStream;
        this.writer = writer;
    }

    private int reconnectionCount = 0;

    private void startReading() {
        Message message;
        Object o;
        try {
            while ((o = inputStream.readObject()) != null) {
                try {
                    message = (Message) o;
                } catch (ClassCastException e) {
                    System.out.println("Problem with serialization");
                    continue;
                }
                reconnectionCount = 0;
                if (message instanceof TextMessage) {
                    TextMessage currentMessage = (TextMessage) message;
                    System.out.println("I've got a new message!");
                    System.out.println(currentMessage.getUser() + " " + currentMessage.getMessage());
                }
                if (message instanceof SerializableImage) {
                    System.out.println("Serializable Image. Ha-ha-ha.");
                }
                processMessageListener.processMessage(message);
            }
        }
        catch (SocketException e) {
            writer.connectionRefused();
        }
        catch (EOFException e) {
            writer.connectionRefused();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        reconnectionCount = 0;
        while (reconnectionCount < 10) {
            System.out.println("There was a try to reconnect!");
            startReading();
            writer.reconnect();
            inputStream = writer.getInputStream();
            ++reconnectionCount;
        }
        writer.connectionRefused();
    }

    public void setProcessMessageListener(ProcessMessageListener processMessageListener) {
        this.processMessageListener = processMessageListener;
    }
}
