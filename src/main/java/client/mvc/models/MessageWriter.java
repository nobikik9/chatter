package client.mvc.models;

import communication.Message;

import java.io.ObjectInputStream;

/**
 * Created by nobik on 5/18/16.
 */
public interface MessageWriter {
    void broadcast(Message message);
    void connectionRefused();
    boolean reconnect();
    ObjectInputStream getInputStream();
}
