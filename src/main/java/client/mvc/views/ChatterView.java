package client.mvc.views;

import client.mvc.controllers.SendMessageListener;
import communication.Message;
import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by nobik on 5/1/16.
 */
public class ChatterView extends JFrame{
    public enum ChatterState {
        WAITING, RUNNING
    }

    ChatterState programState;
    ConnectToServerWindow connectWindow;
    MainWindow mainWindow;

    public ChatterView() {
        initGUI();
    }

    private void initGUI() {
        LayoutManager manager = new CardLayout();
        connectWindow = new ConnectToServerWindow();
        mainWindow = new MainWindow();
        manager.addLayoutComponent("connectWindow", connectWindow);
        manager.addLayoutComponent("mainWindow", mainWindow);
        setWaitingState();
    }

    public String getIP() {
        return connectWindow.getIP();
    }

    public String getPort() {
        return connectWindow.getPort();
    }

    public String getUserName() {
        return connectWindow.getUserName();
    }

    public void setRunningState() {
        if (programState != ChatterState.RUNNING) {
            programState = ChatterState.RUNNING;
            connectWindow.setVisible(false);
            mainWindow.setUser(getUserName());
            mainWindow.setVisible(true);
        }
    }

    public void setWaitingState() {
        if (programState != ChatterState.WAITING) {
            programState = ChatterState.WAITING;
            mainWindow.setVisible(false);
            connectWindow.setVisible(true);
        }
    }

    public void clearScreen() {
        mainWindow.clearScreen();
    }

    public void notifyError(String message, String title) {
        JOptionPane.showMessageDialog(connectWindow, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public void setConnectToServerListener(ActionListener listener) {
        connectWindow.setConnectToServerListener(listener);
    }

    public void processMessage(Message message) {
        mainWindow.processMessage(message);
    }

    public void setSendMessageListener(SendMessageListener sendMessageListener) {
        mainWindow.setSendMessageListener(sendMessageListener);
    }

    public void exit() {
        System.exit(0);
    }
}
