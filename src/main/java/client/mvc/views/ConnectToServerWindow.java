package client.mvc.views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by nobik on 5/17/16.
 */
public class ConnectToServerWindow extends JFrame {
    private JButton connectButton;
    private JButton exitButton;
    private JTextField ipTextField;
    private JTextField portTextField;
    private JPanel contentPane;
    private JTextField usernameTextField;

    public ConnectToServerWindow() {
        initGUI();
    }

    private void initGUI() {
        setContentPane(contentPane);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        pack();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        setLocation((int)toolkit.getScreenSize().getWidth() / 2 - getWidth() / 2,
                (int)toolkit.getScreenSize().getHeight() / 2 - getHeight() / 2);

        exitButton.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
    }

    public void setConnectToServerListener(ActionListener listener) {
        connectButton.addActionListener(listener);
    }

    public String getIP() {
        return ipTextField.getText();
    }

    public String getPort() {
        return portTextField.getText();
    }

    public String getUserName() {
        return usernameTextField.getText();
    }
}
