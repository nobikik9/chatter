package client.mvc.views;

import client.mvc.controllers.SendMessageListener;
import com.sun.prism.paint.*;
import communication.DrawLineMessage;
import communication.Message;
import communication.SerializableImage;
import communication.TextMessage;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.*;
import java.awt.*;
import java.awt.Color;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.Serializable;
import java.nio.Buffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by nobik on 5/17/16.
 */
public class MainWindow extends JFrame {
    /* Communication with model */

    private SendMessageListener sendMessageListener;

    /* View */

    private JButton sendButton;
    private JTextField messageField;
    private JPanel contentPane;
    private JTextPane messageArea;
    private JScrollPane drawingPanel;
    private JLabel drawingLabel;
    private JButton penColorButton;
    private JSlider penWidthSlider;
    private JButton fontColorButton;
    private JSpinner fontSizeSpinner;
    private JButton clearButton;
    private BufferedImage paint;

    /*
    * Rendering hints for better drawing!
    * */
    private RenderingHints renderingHints;
    private Map<RenderingHints.Key, Object> hints = new HashMap<>();

    /*
    * Colors for pen and for text
    * */
    private Color penColor = Color.BLACK;
    private Color textColor = Color.BLACK;

    /*
    * Another attributes of pen and font
    * */
    private static final int MINIMUM_FONT_SIZE = 10;
    private static final int MAXIMUM_FONT_SIZE = 20;
    private static final int DEFAULT_FONT_SIZE = 12;
    private static final float DEFAULT_PEN_WIDTH = 14;

    private float penWidth = DEFAULT_PEN_WIDTH;
    private int fontSize = DEFAULT_FONT_SIZE;

    private Point previousPoint = null;

    private String user = null;

    Random random = new Random();

    public MainWindow() {
        initGui();
        initMenu();
        initListeners();
    }

    void initGui() {
        setupRenderingHints();
        setupContentPane();
        setupDrawingPanel();
    }

    void initMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenu exit = new JMenu("Exit");
        JMenuItem saveItem = new JMenuItem("Save");
        JMenuItem exitItem = new JMenuItem("Exit");
        // Simple saver of our image
        saveItem.addActionListener((ActionEvent e) -> {
            JFileChooser saveFile = new JFileChooser();
            saveFile.setFileFilter(new FileNameExtensionFilter("png", "png"));
            if (saveFile.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                try {
                    File image = saveFile.getSelectedFile();
                    synchronized (paint) {
                        ImageIO.write(paint, "png", image);
                    }
                }
                catch (Exception e1) {
                    JOptionPane.showMessageDialog(this, "Cannot save file", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        exitItem.addActionListener((ActionEvent e) -> System.exit(0));
        file.add(saveItem);
        exit.add(exitItem);
        menuBar.add(file);
        menuBar.add(exit);
        setJMenuBar(menuBar);
    }

    private void setupRenderingHints() {
        Map<RenderingHints.Key, Object> hintsMap = new HashMap<>();
        hintsMap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        hintsMap.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        hintsMap.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        renderingHints = new RenderingHints(hintsMap);
    }

    private void setupContentPane() {
        setContentPane(contentPane);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();

        // Drawing borders
        messageArea.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1, true));
        drawingPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1, true));

        // Centering
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        setLocation((int)toolkit.getScreenSize().getWidth() / 2 - getWidth() / 2,
                (int)toolkit.getScreenSize().getHeight() / 2 - getHeight() / 2);

        fontSizeSpinner.setModel(new SpinnerNumberModel(DEFAULT_FONT_SIZE, MINIMUM_FONT_SIZE, MAXIMUM_FONT_SIZE, 1));
        ((JSpinner.DefaultEditor)fontSizeSpinner.getEditor()).getTextField().setEditable(false);

        DefaultCaret caret = (DefaultCaret) messageArea.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        setResizable(false);
    }

    private void setupDrawingPanel() {
        paint = new BufferedImage(drawingPanel.getWidth(), drawingLabel.getHeight(), BufferedImage.TYPE_INT_RGB);
        System.out.println("Hello " + drawingPanel.getWidth() + " " + drawingPanel.getHeight());
        // Making our screen white
        for (int i = 0; i < paint.getWidth(); ++i) {
            for (int j = 0; j < paint.getHeight(); ++j) {
                paint.setRGB(i, j, Color.WHITE.getRGB());
            }
        }
        drawingLabel.setIcon(new ImageIcon(paint));
        Graphics2D graphics = paint.createGraphics();
        graphics.setColor(Color.WHITE);
        graphics.dispose();
        drawingLabel.repaint();
    }

    private void initListeners() {
        penColorButton.addActionListener((ActionEvent e) -> {
            Color current = JColorChooser.showDialog(MainWindow.this, "Choose pen color", Color.BLACK);
            if (current != null) {
                penColorButton.setBackground(current);
                penColor = current;
            }
        });

        fontColorButton.addActionListener((ActionEvent e) -> {
            Color current = JColorChooser.showDialog(MainWindow.this, "Choose text color", Color.BLACK);
            if (current != null) {
                fontColorButton.setBackground(current);
                textColor = current;
            }
        });

        penWidthSlider.addChangeListener((ChangeEvent e) -> {
            penWidth = penWidthSlider.getValue() / 50f * DEFAULT_PEN_WIDTH;
        });

        fontSizeSpinner.addChangeListener((ChangeEvent e) -> {
            fontSize = (Integer)fontSizeSpinner.getValue();
        });

        clearButton.addActionListener((ActionEvent e) -> {
            clearScreen();
        });

        ActionListener textSentListener = (ActionEvent e) -> {
            String message = messageField.getText();
            if (message.length() > 0) {
                sendMessage(new TextMessage(user, message, textColor, fontSize));
                //appendMessage(new TextMessage("test", message, textColor, fontSize));
                messageField.setText(null);
            }
        };

        messageField.addActionListener(textSentListener);
        sendButton.addActionListener(textSentListener);

        drawingLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                previousPoint = new Point(e.getX(), e.getY());
                //drawLine(new DrawLineMessage("test", previousPoint, previousPoint, penColor, penWidth));
                sendMessage(new DrawLineMessage(user, previousPoint, previousPoint, penColor, penWidth));
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                //drawLine(new DrawLineMessage(user, previousPoint, new Point(e.getX(), e.getY()), penColor, penWidth));
                sendMessage(new DrawLineMessage(user, previousPoint, new Point(e.getX(), e.getY()), penColor, penWidth));
                previousPoint = null;
            }
        });

        drawingLabel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                Point currentPoint = new Point(e.getX(), e.getY());
                //drawLine(new DrawLineMessage(user, previousPoint, currentPoint, penColor, penWidth));
                if (distance(previousPoint, currentPoint) > 2) {
                    sendMessage(new DrawLineMessage(user, previousPoint, currentPoint, penColor, penWidth));
                    previousPoint = currentPoint;
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
            }
        });
    }

    private double distance(Point a, Point b) {
        double dx = a.getX() - b.getX();
        double dy = a.getY() - b.getY();
        return Math.sqrt(dx*dx+dy*dy);
    }

    synchronized void drawLine(DrawLineMessage message) {
        Graphics2D graphics2D = paint.createGraphics();
        graphics2D.setRenderingHints(renderingHints);
        graphics2D.setColor(message.getColor());
        graphics2D.setStroke(new BasicStroke(message.getWidth(),
                            BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND,1.7f));
        graphics2D.drawLine((int)message.getStartPoint().getX(), (int)message.getStartPoint().getY(),
                            (int)message.getEndPoint().getX(), (int)message.getEndPoint().getY());
        graphics2D.dispose();
        drawingLabel.repaint();
    }

    synchronized void appendMessage(TextMessage message) {
        Color [] w = {Color.RED, Color.BLUE, Color.MAGENTA};
        appendTextToMessageArea(message.getUser() + " ~: ", w[random.nextInt(w.length)], DEFAULT_FONT_SIZE);
        appendTextToMessageArea(message.getMessage() + "\n", message.getColor(), message.getFontSize());
    }

    void appendTextToMessageArea(String message, Color color, int fontSize) {
        StyledDocument styledDocument = messageArea.getStyledDocument();
        StyleContext styleContext = StyleContext.getDefaultStyleContext();
        AttributeSet attributeSet = styleContext.addAttribute(SimpleAttributeSet.EMPTY,
                StyleConstants.Foreground,
                color);
        attributeSet = styleContext.addAttribute(attributeSet, StyleConstants.FontSize, fontSize);

        try {
            styledDocument.insertString(messageArea.getDocument().getLength(), message, attributeSet);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    synchronized void processMessage(Message message) {
        if (message == null) {
            return;
        }
        if (message instanceof TextMessage)
            appendMessage((TextMessage) message);
        if (message instanceof DrawLineMessage)
            drawLine((DrawLineMessage) message);
        if (message instanceof SerializableImage) {
            BufferedImage image = ((SerializableImage) message).getPaint();
            if (image == null) System.out.println("dupa");
            if (image == null) return;
            paint = image;
            drawingLabel.setIcon(new ImageIcon(paint));
            drawingLabel.repaint();
        }
    }

    public void setSendMessageListener(SendMessageListener sendMessageListener) {
        this.sendMessageListener = sendMessageListener;
    }

    public void sendMessage(Message message) {
        if (message instanceof DrawLineMessage)
            processMessage(message);
        sendMessageListener.sendMessage(message);
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void clearScreen() {
        sendMessage(new DrawLineMessage("test", new Point(0, 0),
                                new Point(paint.getWidth(), paint.getHeight()), Color.WHITE, 10000f));
    }
}
