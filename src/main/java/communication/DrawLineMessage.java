package communication;

import java.awt.*;
import java.io.Serializable;

/**
 * Created by nobik on 5/26/16.
 */
public class DrawLineMessage extends Message implements Serializable{
    private final Point start, end;
    private final float width;

    public DrawLineMessage(String user, Point start, Point end, Color color, float width) {
        this.user = user;
        this.start = start;
        this.end = end;
        this.color = color;
        this.width = width;
    }

    public Point getStartPoint() {
        return start;
    }

    public Point getEndPoint() {
        return end;
    }

    public float getWidth() {
        return width;
    }
}
