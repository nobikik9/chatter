package communication;

import java.awt.*;
import java.io.Serializable;

/**
 * Created by nobik on 5/4/16.
 */
public abstract class Message implements Serializable {
    String user;
    Color color;

    public String getUser() {
        return user;
    }

    public Color getColor() {
        return color;
    }
}
