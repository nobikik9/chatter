package communication;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Created by nobik on 6/13/16.
 */
public class SerializableImage extends Message implements Serializable {
    transient BufferedImage image;

    public SerializableImage(BufferedImage image) {
        this.image = image;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        ImageIO.write(image, "jpeg", out);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        image = ImageIO.read(in);
    }

    public Color getColor() {
        throw new UnsupportedOperationException();
    }

    public BufferedImage getPaint() {
        return image;
    }
}
