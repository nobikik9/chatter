package communication;

import java.awt.*;
import java.io.Serializable;

/**
 * Created by nobik on 5/26/16.
 */
public class TextMessage extends Message implements Serializable {
    private final String message;
    private final int fontSize;

    public TextMessage(String user, String message, Color color, int fontSize) {
        this.user = user;
        this.message = message;
        this.color = color;
        this.fontSize = fontSize;
    }

    public String getMessage() {
        return message;
    }

    public int getFontSize() {
        return fontSize;
    }
}
