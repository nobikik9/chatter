package server.mvc;

import server.mvc.controllers.ServerController;
import server.mvc.models.ServerModel;
import server.mvc.views.ServerView;

import javax.swing.*;

/**
 * Created by nobik on 5/4/16.
 */
public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            ServerModel model = new ServerModel();
            ServerView view = new ServerView();
            new ServerController(model, view);
            view.setVisible(true);
        });
    }
}
