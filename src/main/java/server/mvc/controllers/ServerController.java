package server.mvc.controllers;

import server.mvc.models.ServerModel;
import server.mvc.views.ServerView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by nobik on 5/4/16.
 */
public class ServerController {
    private ServerModel model;
    private ServerView view;

    public ServerController(ServerModel model, ServerView view) {
        this.model = model;
        this.view = view;

        view.setIP(model.getIP());
        view.setPort(model.getPort());
        view.setStartServerListener(new ServerStartListener());
        view.setStopServerListener(new ServerStopListener());
        view.setExitListener(new ExitListener());
    }

    public class ServerStartListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            model.initServer();
            view.changeState(true);
            System.out.println("There was a try to start server");
        }
    }

    public class ServerStopListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            model.stopServer();
            System.out.println("There was a try to stop server");
            view.changeState(false);
        }
    }

    public class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            model.stopServer();
            view.exit();
        }
    }
}
