package server.mvc.models;

import communication.Message;

/**
 * Created by nobik on 5/12/16.
 */
public interface BroadcastCallback {
    void broadcast(Message message);
}
