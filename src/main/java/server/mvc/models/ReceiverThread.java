package server.mvc.models;

import communication.DrawLineMessage;
import communication.Message;
import communication.SerializableImage;
import communication.TextMessage;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.List;

/**
 * Created by nobik on 5/12/16.
 */
class ReceiverThread extends Thread implements BroadcastCallback {
    private ServerSocket serverSocket;
    private final List<UserThread> users = new ArrayList<UserThread>();
    private final List<Message> messages = new LinkedList<>();
    private final BufferedImage paint;
    private RenderingHints renderingHints;

    ReceiverThread(Integer port, BufferedImage paint) {
        this.paint = paint;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void setupRenderingHints() {
        Map<RenderingHints.Key, Object> hintsMap = new HashMap<>();
        hintsMap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        hintsMap.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        hintsMap.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        renderingHints = new RenderingHints(hintsMap);
    }

    @Override
    public void run() {
        try {
            setupRenderingHints();
            while (!isInterrupted()) {
                Socket userSocket = serverSocket.accept();
                synchronized (users) {
                    UserThread newThread = new UserThread(userSocket, this);
                    newThread.start();
                    users.add(newThread);
                    System.out.println("User was added successfully!");

                    synchronized (this) {
                        for (Message message : messages) {
                            newThread.sendMessage(message);
                        }
                        newThread.sendMessage(new SerializableImage(paint));
                    }
                }
            }
            for (UserThread user : users) {
                user.interrupt();
            }
        } catch (IOException e) {
            System.out.println("Server was stopped!");
        }
    }

    public void stopServer() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (UserThread user : users) {
            user.stopUserThread();
        }
        users.clear();
        Thread.currentThread().interrupt();
    }

    private void deleteDiedUsers() {
        List<UserThread> temp = new ArrayList<UserThread>();
        for (UserThread thread : users) {
            if (!thread.isAlive()) {
                temp.add(thread);
            }
        }
        users.removeAll(temp);
    }

    /* This function will broadcast message to all connected users */
    // Must be synchronized!
    public synchronized void broadcast(Message message) {
        deleteDiedUsers();
        for (UserThread thread : users) {
            if (message instanceof TextMessage) {
                messages.add(message);
                if (messages.size() > 30) messages.remove(messages.get(0));
            }
            processMessage(message);
            thread.sendMessage(message);
        }
    }

    synchronized void processMessage(Message message) {
        if (message instanceof DrawLineMessage)
            drawLine((DrawLineMessage) message);
    }

    synchronized void drawLine(DrawLineMessage message) {
        Graphics2D graphics2D = paint.createGraphics();
        graphics2D.setRenderingHints(renderingHints);
        graphics2D.setColor(message.getColor());
        graphics2D.setStroke(new BasicStroke(message.getWidth(),
                BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND,1.7f));
        graphics2D.drawLine((int)message.getStartPoint().getX(), (int)message.getStartPoint().getY(),
                (int)message.getEndPoint().getX(), (int)message.getEndPoint().getY());
        graphics2D.dispose();
    }
}

