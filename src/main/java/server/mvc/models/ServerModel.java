package server.mvc.models;

import communication.Message;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.*;
import java.util.Enumeration;

/**
 * Created by nobik on 5/4/16.
 */
public class ServerModel {
    public static final int PAINT_WIDTH = 600;
    public static final int PAINT_HEIGHT = 400;

    public static final int PORT = 6789;
    private boolean isServerRunning = false;
    private ReceiverThread receiver;
    private BufferedImage paint = null;

    public ServerModel() {
    }

    public void initServer() {
        if (!isServerRunning) {
            clearImage();
            receiver = new ReceiverThread(PORT, paint);
            receiver.start();
        }
        isServerRunning = true;
    }

    public void stopServer() {
        if (isServerRunning) {
            receiver.stopServer();
        }
        isServerRunning = false;
    }

    private void clearImage() {
        paint = new BufferedImage(PAINT_WIDTH, PAINT_HEIGHT, BufferedImage.TYPE_INT_RGB);
        // Making our screen white
        for (int i = 0; i < paint.getWidth(); ++i) {
            for (int j = 0; j < paint.getHeight(); ++j) {
                paint.setRGB(i, j, Color.WHITE.getRGB());
            }
        }
    }

    /*
    * There is a problem with IPs.
    * One computer may have several ones.
    * That's wy we should iterate over all possible and find out which is site local
    * */
    public String getIP() {
        try {
            String result = null;
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            while (e.hasMoreElements()) {
                NetworkInterface networkInterface = (NetworkInterface)e.nextElement();
                Enumeration addresses = networkInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress address = (InetAddress)addresses.nextElement();
                    if (address.isLoopbackAddress()) {
                        continue;
                    }
                    if (address.isSiteLocalAddress()) {
                        return address.getHostAddress();
                    }
                    result = address.getHostAddress();
                }

            }
            if (result != null) {
                return result;
            }
        } catch (SocketException e1) {
            e1.printStackTrace();
        }
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "Cannot determine your IP address!";
    }

    /*
    * Currently we using only this PORT.
    * It may change lately
    * */
    public Integer getPort() {
        return PORT;
    }



}
