package server.mvc.models;

import communication.DrawLineMessage;
import communication.Message;
import communication.TextMessage;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by nobik on 5/12/16.
 */

public class UserThread extends Thread {
    private final Socket userSocket;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private BroadcastCallback broadcaster;
    private String name;

    UserThread(Socket userSocket, BroadcastCallback broadcaster) {
        this.userSocket = userSocket;
        this.broadcaster = broadcaster;
        setDaemon(true);
        try {
            outputStream = new ObjectOutputStream(userSocket.getOutputStream());
            outputStream.flush();
            inputStream = new ObjectInputStream(userSocket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void sendMessage(Message toSend) {
        try {
            outputStream.writeObject(toSend);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Sending message failed!");
        }
    }

    @Override
    public void run() {
        Message currentMessage = null;
        try {
            currentMessage = (Message)inputStream.readObject();
            name = currentMessage.getUser();
            Object o;
            while ((o = inputStream.readObject()) != null && !isInterrupted()) {
                broadcaster.broadcast((Message)o);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Bye-bye");
        try {
            userSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return name;
    }

    public void stopUserThread() {
        try {
            inputStream.close();
        } catch (IOException e) {
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }
        try {
            userSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
