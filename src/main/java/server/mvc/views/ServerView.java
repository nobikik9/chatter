package server.mvc.views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by nobik on 5/17/16.
 */
public class ServerView extends JFrame {
    private JButton exitButton;
    private JButton runServerButton;
    private JTextField textFieldPort;
    private JTextField textFieldIP;
    private JPanel contentPane;
    private JButton stopServerButton;

    public ServerView() {
        setContentPane(contentPane);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();

        // Centering window
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenDimensions = toolkit.getScreenSize();
        setLocation((int)screenDimensions.getWidth() / 2 - getWidth() / 2,
                    (int)screenDimensions.getHeight() / 2 - getHeight() / 2);
    }

    public void setIP(String ip) {
        textFieldIP.setText(ip);
    }

    public void setPort(Integer port) {
        textFieldPort.setText(port.toString());
    }

    public void setStartServerListener(ActionListener listener) {
        runServerButton.addActionListener(listener);
    }

    public void setStopServerListener(ActionListener listener) {
        stopServerButton.addActionListener(listener);
    }

    public void setExitListener(ActionListener listener)  {
        exitButton.addActionListener(listener);
    }

    public void changeState(boolean isRunning) {
        if (isRunning)
            runServerButton.setEnabled(!isRunning);
        stopServerButton.setEnabled(isRunning);
    }

    public void exit() {
        stopServerButton.getActionListeners()[0].actionPerformed(new ActionEvent(new Object(),1,"exit"));
        dispose();
    }
}
